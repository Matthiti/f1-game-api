FROM php:8.0-fpm-alpine

RUN apk update && apk add --no-cache curl mysql-client

RUN docker-php-ext-install pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www

COPY composer.* ./

RUN composer install --optimize-autoloader --no-dev

COPY . .

RUN chown -R www-data:www-data .

USER www-data

EXPOSE 9000
