<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => '/api/v1'], function() use ($router) {
    $router->group(['prefix' => 'auth'], function() use ($router) {
        $router->post('login', 'AuthController@login');
    });

    $router->group(['prefix' => 'players'], function() use ($router) {
        $router->get('', 'PlayerController@all');
        $router->get('{id}', 'PlayerController@find');
    });

    $router->group(['prefix' => 'drivers'], function() use ($router) {
        $router->get('', 'DriverController@all');
        $router->get('{id}', 'DriverController@find');
    });

    $router->group(['prefix' => 'formulas'], function() use ($router) {
        $router->get('', 'FormulaController@all');
        $router->get('{id}', 'FormulaController@find');
    });

    $router->group(['prefix' => 'tracks'], function() use ($router) {
        $router->get('', 'TrackController@all');
        $router->get('{id}', 'TrackController@find');

        $router->group(['prefix' => '{trackID}/laps'], function() use ($router) {
            $router->get('', 'LapController@all');
            $router->get('filters', 'LapController@filters');
            $router->get('{lapID}', 'LapController@find');
        });
    });

    $router->group(['prefix' => 'tyres'], function() use ($router) {
        $router->get('', 'TyreController@all');
        $router->get('{id}', 'TyreController@find');
    });

    $router->group(['prefix' => 'teams'], function() use ($router) {
        $router->get('', 'TeamController@all');
        $router->get('{id}', 'TeamController@find');
    });

    $router->group(['prefix' => 'weather'], function() use ($router) {
        $router->get('', 'WeatherController@all');
        $router->get('{id}', 'WeatherController@find');
    });

    $router->group(['prefix' => 'session-types'], function() use ($router) {
        $router->get('', 'SessionTypeController@all');
        $router->get('{id}', 'SessionTypeController@find');
    });

    $router->group(['prefix' => 'leaderboard'], function() use ($router) {
        $router->get('', 'LeaderboardController@leaderboard');
        $router->get('filters', 'LeaderboardController@filters');
    });

    $router->group(['middleware' => 'jwt'], function() use ($router) {
        $router->group(['prefix' => 'auth'], function() use ($router) {
            $router->get('me', 'AuthController@me');
        });

        $router->group(['prefix' => 'players'], function() use ($router) {
            $router->post('', 'PlayerController@create');
            $router->put('{id}', 'PlayerController@update');
            $router->delete('{id}', 'PlayerController@delete');
        });

        $router->group(['prefix' => 'tracks/{trackID}/laps/{id}'], function() use ($router) {
            $router->put('', 'LapController@update');
            $router->delete('', 'LapController@delete');
        });

        $router->group(['prefix' => 'session'], function() use ($router) {
            $router->get('', 'SessionController@get');
            $router->post('', 'SessionController@set');
        });

        $router->group(['prefix' => 'auth/api-tokens'], function() use ($router) {
            $router->get('', 'ApiTokenController@all');
            $router->post('', 'ApiTokenController@create');
            $router->group(['prefix' => '{id}'], function() use ($router) {
                $router->get('', 'ApiTokenController@find');
                $router->put('', 'ApiTokenController@update');
                $router->delete('', 'ApiTokenController@delete');
            });
        });
    });

    $router->group(['middleware' => 'api-token'], function() use ($router) {
        $router->group(['prefix' => 'tracks/{trackID}/laps'], function() use ($router) {
            $router->post('', 'LapController@create');
        });
    });
});
