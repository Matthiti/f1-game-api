<?php

use App\Models\Driver;
use Illuminate\Database\Migrations\Migration;

class AddCustomDriverId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $driver = new Driver();
        $driver->id = 100;
        $driver->name = 'Custom driver';
        $driver->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Driver::query()->findOrFail(100)->delete();
    }
}
