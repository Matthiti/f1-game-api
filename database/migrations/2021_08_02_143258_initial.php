<?php

use App\Models\Driver;
use App\Models\Formula;
use App\Models\SessionType;
use App\Models\Team;
use App\Models\Track;
use App\Models\Tyre;
use App\Models\Weather;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Initial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('created_at');
            $table->integer('updated_at');
        });

        Schema::create('api_tokens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('token_hash')->index();
            $table->unsignedBigInteger('created_at');
        });

        Schema::create('players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->unique();
        });

        Schema::create('tracks', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('name');
            $table->string('gp_name');
        });

        $tracks = [
            [0, 'Melbourne', 'Australian Grand Prix'],
            [1, 'Paul Richard', 'French Grand Prix'],
            [2, 'Shanghai', 'Chinese Grand Prix'],
            [3, 'Sakhir (Bahrain)', 'Bahrain Grand Prix'],
            [4, 'Catalunya', 'Spanish Grand Prix'],
            [5, 'Monaco', 'Monaco Grand Prix'],
            [6, 'Montreal', 'Canadian Grand Prix'],
            [7, 'Silverstone', 'British Grand Prix'],
            [9, 'Hungaroring', 'Hungarian Grand Prix'],
            [10, 'Spa', 'Belgium Grand Prix'],
            [11, 'Monza', 'Italian Grand Prix'],
            [12, 'Singapore', 'Singapore Grand Prix'],
            [13, 'Suzuka', 'Japanese Grand Prix'],
            [14, 'Abu Dhabi', 'Abu Dhabi Grand Prix'],
            [15, 'Texas', 'United States Grand Prix'],
            [16, 'Brazil', 'Brazilian Grand Prix'],
            [17, 'Austria', 'Austrian Grand Prix'],
            [18, 'Sochi', 'Russian Grand Prix'],
            [19, 'Mexico', 'Mexican Grand Prix'],
            [20, 'Baku (Azerbaijan)', 'Azerbaijan Grand Prix'],
            [21, 'Sakhir Short', 'Short Bahrain Grand Prix'],
            [22, 'Silversone Short', 'Short British Grand Prix'],
            [23, 'Texas', 'Short United States Grand Prix'],
            [24, 'Suzuka Short', 'Short Japanese Grand Prix'],
            [25, 'Hanoi', 'Vietnamese Grand Prix'],
            [26, 'Zandvoort', 'Dutch Grand Prix']
        ];

        foreach ($tracks as $t) {
            $track = new Track();
            $track->id = $t[0];
            $track->name = $t[1];
            $track->gp_name = $t[2];
            $track->save();
        }

        Schema::create('drivers', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('name');
        });

        $drivers = [
            [0, 'Carlos Sainz'],
            [1, 'Daniil Kvyat'],
            [2, 'Daniel Ricciardo'],
            [6, 'Kimi Räikkönen'],
            [7, 'Lewis Hamilton'],
            [9, 'Max Verstappen'],
            [10, 'Nico Hulkenberg'],
            [11, 'Kevin Magnussen'],
            [12, 'Romain Grosjean'],
            [13, 'Sebastian Vettel'],
            [14, 'Sergio Perez'],
            [15, 'Valtteri Bottas'],
            [17, 'Esteban Ocon'],
            [19, 'Lance Stroll'],
            [20, 'Arron Barnes'],
            [21, 'Martin Giles'],
            [22, 'Alex Murray'],
            [23, 'Lucas Roth'],
            [24, 'Igor Correia'],
            [25, 'Sophie Levasseur'],
            [26, 'Jonas Schiffer'],
            [27, 'Alain Forest'],
            [28, 'Jay Letourneau'],
            [29, 'Esto Saari'],
            [30, 'Yasar Atiyeh'],
            [31, 'Callisto Calabresi'],
            [32, 'Naota Izum'],
            [33, 'Howard Clarke'],
            [34, 'Wilhelm Kaufmann'],
            [35, 'Marie Laursen'],
            [36, 'Flavio Nieves'],
            [37, 'Peter Belousov'],
            [38, 'Klimek Michalski'],
            [39, 'Santiago Moreno'],
            [40, 'Benjamin Coppens'],
            [41, 'Noah Visser'],
            [42, 'Gert Waldmuller'],
            [43, 'Julian Quesada'],
            [44, 'Daniel Jones'],
            [45, 'Artem Markelov'],
            [46, 'Tadasuke Makino'],
            [47, 'Sean Gelael'],
            [48, 'Nyck De Vries'],
            [49, 'Jack Aitken'],
            [50, 'George Russell'],
            [51, 'Maximilian Günther'],
            [52, 'Nirei Fukuzumi'],
            [53, 'Luca Ghiotto'],
            [54, 'Lando Norris'],
            [55, 'Sérgio Sette Câmara'],
            [56, 'Louis Delétraz'],
            [57, 'Antonio Fuoco'],
            [58, 'Charles Leclerc'],
            [59, 'Pierre Gasly'],
            [62, 'Alexander Albon'],
            [63, 'Nicholas Latifi'],
            [64, 'Dorian Boccolacci'],
            [65, 'Niko Kari'],
            [66, 'Roberto Merhi'],
            [67, 'Arjun Maini'],
            [68, 'Alessio Lorandi'],
            [69, 'Ruben Meijer'],
            [70, 'Rashid Nair'],
            [71, 'Jack Tremblay'],
            [74, 'Antonio Giovinazzi'],
            [75, 'Robert Kubica'],
            [78, 'Nobuharu Matsushita'],
            [79, 'Nikita Mazepin'],
            [80, 'Guanyu Zhou'],
            [81, 'Mick Schumacher'],
            [82, 'Callum Ilott'],
            [83, 'Juan Manuel Correa'],
            [84, 'Jordan King'],
            [85, 'Mahaveer Raghunathan'],
            [86, 'Tatiana Calderón'],
            [87, 'Anthoine Hubert'],
            [88, 'Giuliano Alesi'],
            [89, 'Ralph Boschung']
        ];

        foreach ($drivers as $d) {
            $driver = new Driver();
            $driver->id = $d[0];
            $driver->name = $d[1];
            $driver->save();
        }

        Schema::create('teams', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('name');
        });

        $teams = [
            [0, 'Mercedes'],
            [1, 'Ferrari'],
            [2, 'Red Bull Racing'],
            [3, 'Williams'],
            [4, 'Racing Point'],
            [6, 'AlphaTauri'],
            [7, 'Haas'],
            [8, 'McLaren'],
            [9, 'Alfa Romeo'],
            [10, 'McLaren 1988'],
            [11, 'McLaren 1991'],
            [12, 'Williams 1992'],
            [13, 'Ferrari 1995'],
            [14, 'Williams 1996'],
            [15, 'McLaren 1998'],
            [16, 'Ferrari 2002'],
            [17, 'Ferrari 2004'],
            [18, 'Renault 2006'],
            [19, 'Ferrari 2007'],
            [20, 'McLaren 2008'],
            [21, 'Red Bull 2010'],
            [255, 'My Team'],
            [31, 'McLaren 1990'],
            [38, 'Williams 2003'],
            [39, 'Brawn 2009'],
            [41, 'F1 Generic car'],
            [42, 'ART Grand Prix'],
            [43, 'Campos Racing'],
            [44, 'Carlin'],
            [45, 'Sauber Junior Team by Charouz'],
            [46, 'DAMS'],
            [47, 'UNI-Virtuosi'],
            [48, 'MP Motorsport'],
            [49, 'PREMA Racing'],
            [5, 'Renault'],
            [50, 'Trident'],
            [51, 'BWT Arden'],
            [53, 'Benetton 1994'],
            [54, 'Benetton 1995'],
            [55, 'Ferrari 2000'],
            [56, 'Jordan 1991'],
            [63, 'Ferrari 1990'],
            [64, 'McLaren 2010'],
            [65, 'Ferrari 2010']
        ];

        foreach ($teams as $t) {
            $team = new Team();
            $team->id = $t[0];
            $team->name = $t[1];
            $team->save();
        }

        Schema::create('weather', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('name');
        });

        $weather = [
            [0, 'Clear'],
            [1, 'Light cloud'],
            [2, 'Overcast'],
            [3, 'Light rain'],
            [4, 'Heavy rain'],
            [5, 'Storm']
        ];

        foreach ($weather as $w) {
            $w_obj = new Weather();
            $w_obj->id = $w[0];
            $w_obj->name = $w[1];
            $w_obj->save();
        }

        Schema::create('formulas', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('name');
        });

        $formulas = [
            [0, 'F1 Modern'],
            [1, 'F1 Classic'],
            [2, 'F2'],
            [3, 'F1 Generic']
        ];

        foreach ($formulas as $f) {
            $formula = new Formula();
            $formula->id = $f[0];
            $formula->name = $f[1];
            $formula->save();
        }

        Schema::create('tyres', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('name');

            $table->unsignedBigInteger('formula_id');
            $table->foreign('formula_id')
                ->references('id')->on('formulas')->onDelete('cascade');
        });

        $tyres = [
            [7, 0, 'Inter'],
            [8, 0, 'Wet'],
            [9, 1, 'Dry'],
            [10, 1, 'Wet'],
            [11, 2, 'Super soft'],
            [12, 2, 'Soft'],
            [13, 2, 'Medium'],
            [14, 2, 'Hard'],
            [15, 2, 'Wet'],
            [16, 0, 'C5'],
            [17, 0, 'C4'],
            [18, 0, 'C3'],
            [19, 0, 'C2'],
            [20, 0, 'C1']
        ];

        foreach ($tyres as $t) {
            $tyre = new Tyre();
            $tyre->id = $t[0];
            $tyre->formula_id = $t[1];
            $tyre->name = $t[2];
            $tyre->save();
        }

        Schema::create('session_types', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('name');
        });

        $sessionTypes = [
            [0, 'Unknown'],
            [1, 'P1'],
            [2, 'P2'],
            [3, 'P3'],
            [4, 'Short P'],
            [5, 'Q1'],
            [6, 'Q2'],
            [7, 'Q3'],
            [8, 'Short Q'],
            [9, 'OSQ'],
            [10, 'R'],
            [11, 'R2'],
            [12, 'Time Trial']
        ];

        foreach ($sessionTypes as $s) {
            $sessionType = new SessionType();
            $sessionType->id = $s[0];
            $sessionType->name = $s[1];
            $sessionType->save();
        }

        Schema::create('laps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('lap_no');
            $table->decimal('time',18, 14)->index();
            $table->float('time_sector_1', 7, 3);
            $table->float('time_sector_2', 7, 3);
            $table->float('time_sector_3', 7, 3);
            $table->bigInteger('timestamp');

            $table->unsignedBigInteger('track_id');
            $table->foreign('track_id')
                ->references('id')->on('tracks')->onDelete('cascade');

            $table->unsignedBigInteger('player_id');
            $table->foreign('player_id')
                ->references('id')->on('players')->onDelete('cascade');

            $table->unsignedBigInteger('driver_id');
            $table->foreign('driver_id')
                ->references('id')->on('drivers')->onDelete('cascade');

            $table->unsignedBigInteger('team_id');
            $table->foreign('team_id')
                ->references('id')->on('teams')->onDelete('cascade');

            $table->unsignedBigInteger('weather_id');
            $table->foreign('weather_id')
                ->references('id')->on('weather')->onDelete('cascade');

            $table->unsignedBigInteger('formula_id');
            $table->foreign('formula_id')
                ->references('id')->on('formulas')->onDelete('cascade');

            $table->unsignedBigInteger('session_type_id');
            $table->foreign('session_type_id')
                ->references('id')->on('session_types')->onDelete('cascade');

            $table->unsignedBigInteger('tyre_id');
            $table->foreign('tyre_id')
                ->references('id')->on('tyres')->onDelete('cascade');

            $table->integer('tyre_age');
            $table->boolean('drs_used');
            $table->boolean('overtake_button_used');
        });

        Schema::create('sessions', function(Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('player_1_id');
            $table->foreign('player_1_id')
                ->references('id')->on('players')->onDelete('cascade');

            $table->unsignedBigInteger('player_2_id')->nullable();
            $table->foreign('player_2_id')
                ->references('id')->on('players')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
        Schema::dropIfExists('laps');
        Schema::dropIfExists('tyres');
        Schema::dropIfExists('formulas');
        Schema::dropIfExists('weathers');
        Schema::dropIfExists('drivers');
        Schema::dropIfExists('tracks');
        Schema::dropIfExists('players');
        Schema::dropIfExists('users');
    }
}
