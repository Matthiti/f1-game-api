<?php

use App\Models\Session;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class SessionPlayer1Nullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE sessions MODIFY player_1_id bigint unsigned');
        if (Session::query()->first() === null) {
            $session = new Session();
            $session->player_1_id = null;
            $session->player_2_id = null;
            $session->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $session = Session::query()->first();
        if ($session !== null && $session->player_1_id === null) {
            $session->delete();
        }

        DB::statement('ALTER TABLE sessions MODIFY player_1_id bigint unsigned NOT NULL');
    }
}
