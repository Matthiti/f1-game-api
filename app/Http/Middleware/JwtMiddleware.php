<?php

namespace App\Http\Middleware;

use App\Exceptions\UnAuthorizedException;
use App\Models\User;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Closure;

class JwtMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        $header = $request->header('X-Authorization');
        if ($header === null) {
            throw new UnAuthorizedException("Missing 'X-Authorization' header");
        }

        if (!str_starts_with($header, 'Bearer')) {
            throw new UnAuthorizedException("Invalid 'X-Authorization' header");
        }

        $token = substr($header, strlen("Bearer "));

        try {
            $decoded = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch (ExpiredException) {
            throw new UnAuthorizedException('Token is expired');
        } catch (\Exception) {
            throw new UnAuthorizedException('Invalid token');
        }

        $user = User::query()->find($decoded->sub);
        if ($user === null) {
            throw new UnAuthorizedException('Unknown user');
        }

        $request->auth = $user;
        return $next($request);
    }
}
