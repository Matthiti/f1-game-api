<?php

namespace App\Http\Middleware;

use App\Exceptions\UnAuthorizedException;
use App\Models\ApiToken;
use Closure;
use Illuminate\Http\Request;

class ApiTokenMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        $header = $request->header('X-API-Token');
        if ($header === null) {
            throw new UnAuthorizedException("Missing 'X-Api-Token' header");
        }

        $tokenHash = hash('sha256', $header);
        $apiToken = ApiToken::query()->where('token_hash', $tokenHash)->first();
        if ($apiToken === null) {
            throw new UnAuthorizedException('Invalid API token');
        }

        return $next($request);
    }
}
