<?php

namespace App\Http\Middleware;

use Closure;

class ResponseWrapperMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if ($response->isSuccessful() && !$response->isEmpty()) {
            $data = $response->getOriginalContent();
            $response->setContent([
                'success' => true,
                'data' => $data
            ]);
        }
        return $response;
    }
}
