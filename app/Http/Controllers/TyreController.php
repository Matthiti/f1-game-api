<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseControllers\BaseReadController;
use App\Models\Tyre;

class TyreController extends BaseReadController
{
    public function __construct()
    {
        parent::__construct(Tyre::class);
    }
}
