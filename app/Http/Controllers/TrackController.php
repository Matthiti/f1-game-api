<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseControllers\BaseReadController;
use App\Models\Track;

class TrackController extends BaseReadController
{
    public function __construct()
    {
        parent::__construct(Track::class);
    }
}
