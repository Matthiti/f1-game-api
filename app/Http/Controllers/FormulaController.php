<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseControllers\BaseReadController;
use App\Models\Formula;

class FormulaController extends BaseReadController
{
    public function __construct()
    {
        parent::__construct(Formula::class);
    }
}
