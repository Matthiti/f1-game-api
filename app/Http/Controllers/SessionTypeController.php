<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseControllers\BaseReadController;
use App\Models\SessionType;

class SessionTypeController extends BaseReadController
{
    public function __construct()
    {
        parent::__construct(SessionType::class);
    }
}
