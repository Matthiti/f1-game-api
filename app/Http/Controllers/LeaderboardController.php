<?php

namespace App\Http\Controllers;

use App\Helpers\Filters;
use App\Models\Driver;
use App\Models\Formula;
use App\Models\Lap;
use App\Models\Player;
use App\Models\SessionType;
use App\Models\Team;
use App\Models\Track;
use App\Models\Tyre;
use App\Models\Weather;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller;

class LeaderboardController extends Controller
{
    public function leaderboard(Request $request)
    {
        $query = DB::table('laps')->select(DB::raw('track_id, min(time) as time'));
        foreach (Filters::keys() as $filter) {
            $values = $request->query($filter);
            if ($values === null) {
                continue;
            }

            if (!is_array($values)) {
                $values = [$values];
            }

            $query->whereIn($filter, $values);
        }

        $leaderboard_laps = $query
            ->groupBy(['track_id'])
            ->get()
            ->keyBy('track_id')
            ->toArray();

        return array_map(function ($track) use ($leaderboard_laps) {
            return [
                'track' => Track::query()->find($track['id']),
                'top' => key_exists($track['id'], $leaderboard_laps)
                    ? Lap::query()->where('time', '=', $leaderboard_laps[$track['id']]->time)->first()
                    : null
            ];
        }, Track::all()->toArray());
    }

    public function filters()
    {
        return array_map(function ($filter) {
            $options = match ($filter['key']) {
                Filters::FILTER_KEY_PLAYER => array_map(
                    fn($player) => ['name' => $player['username'], 'value' => $player['id']],
                    $this->getFilterOptions(Player::class, $filter['key'])
                ),
                Filters::FILTER_KEY_DRIVER => array_map(
                    fn($driver) => ['name' => $driver['name'], 'value' => $driver['id']],
                    $this->getFilterOptions(Driver::class, $filter['key'])
                ),
                Filters::FILTER_KEY_TEAM => array_map(
                    fn($team) => ['name' => $team['name'], 'value' => $team['id']],
                    $this->getFilterOptions(Team::class, $filter['key'])
                ),
                Filters::FILTER_KEY_FORMULA => array_map(
                    fn($formula) => ['name' => $formula['name'], 'value' => $formula['id']],
                    $this->getFilterOptions(Formula::class, $filter['key'])
                ),
                Filters::FILTER_KEY_SESSION_TYPE => array_map(
                    fn($sessionType) => ['name' => $sessionType['name'], 'value' => $sessionType['id']],
                    $this->getFilterOptions(SessionType::class, $filter['key'])
                ),
                Filters::FILTER_KEY_WEATHER => array_map(
                    fn($weather) => ['name' => $weather['name'], 'value' => $weather['id']],
                    $this->getFilterOptions(Weather::class, $filter['key'])
                ),
                Filters::FILTER_KEY_TYRE => array_map(
                    fn($tyre) => ['name' => $tyre['name'], 'value' => $tyre['id']],
                    $this->getFilterOptions(Tyre::class, $filter['key'])
                ),
                Filters::FILTER_KEY_DRS => array_map(
                    fn($drsUsed) => ['name' => $drsUsed ? 'Yes' : 'No', 'value' => (int) $drsUsed],
                    Lap::query()
                        ->select('drs_used')
                        ->distinct()
                        ->get()
                        ->pluck('drs_used')
                        ->toArray()
                ),
                Filters::FILTER_KEY_OVERTAKE_BUTTON => array_map(
                    fn($overtakeButtonUsed) => ['name' => $overtakeButtonUsed ? 'Yes' : 'No', 'value' => (int)$overtakeButtonUsed],
                    Lap::query()
                        ->select('overtake_button_used')
                        ->distinct()
                        ->get()
                        ->pluck('overtake_button_used')
                        ->toArray()
                ),
                default => [],
            };

            return array_merge($filter, ['options' => $options]);
        }, Filters::all());
    }

    private function getFilterOptions(string $model, string $fk)
    {
        return $model::query()->distinct()->whereIn('id', function ($query) use ($fk) {
            $query->select($fk)
                ->from('laps');
        })->get()->toArray();
    }
}
