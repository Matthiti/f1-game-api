<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidRequestException;
use App\Exceptions\InvalidSessionException;
use App\Exceptions\ModelNotFoundException;
use App\Helpers\Filters;
use App\Models\Driver;
use App\Models\Formula;
use App\Models\Lap;
use App\Models\Player;
use App\Models\SessionType;
use App\Models\Team;
use App\Models\Track;
use App\Models\Tyre;
use App\Models\Weather;
use App\Services\SessionService;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class LapController extends Controller
{
    private SessionService $sessionService;

    public function __construct(SessionService $sessionService)
    {
        $this->sessionService = $sessionService;
    }

    public function all(int $trackID, Request $request)
    {
        $this->checkTrack($trackID);
        $laps = Lap::query()
            ->where('track_id', $trackID)
            ->orderBy('time');

        foreach (Filters::keys() as $filter) {
            $values = $request->query($filter);
            if ($values === null) {
                continue;
            }

            if (!is_array($values)) {
                $values = [$values];
            }

            $laps->whereIn($filter, $values);
        }

        if ($request->query('limit') !== null) {
            $laps->limit($request->query('limit'));

            if ($request->query('offset') !== null) {
                $laps->offset($request->query('offset'));
            }
        }

        return $laps->get();
    }

    public function filters(int $trackID)
    {
        $this->checkTrack($trackID);

        return array_map(function ($filter) use ($trackID) {
            $options = match ($filter['key']) {
                Filters::FILTER_KEY_PLAYER => array_map(
                    fn($player) => ['name' => $player['username'], 'value' => $player['id']],
                    $this->getFilterOptions(Player::class, $filter['key'], $trackID)
                ),
                Filters::FILTER_KEY_DRIVER => array_map(
                    fn($driver) => ['name' => $driver['name'], 'value' => $driver['id']],
                    $this->getFilterOptions(Driver::class, $filter['key'], $trackID)
                ),
                Filters::FILTER_KEY_TEAM => array_map(
                    fn($team) => ['name' => $team['name'], 'value' => $team['id']],
                    $this->getFilterOptions(Team::class, $filter['key'], $trackID)
                ),
                Filters::FILTER_KEY_FORMULA => array_map(
                    fn($formula) => ['name' => $formula['name'], 'value' => $formula['id']],
                    $this->getFilterOptions(Formula::class, $filter['key'], $trackID)
                ),
                Filters::FILTER_KEY_SESSION_TYPE => array_map(
                    fn($sessionType) => ['name' => $sessionType['name'], 'value' => $sessionType['id']],
                    $this->getFilterOptions(SessionType::class, $filter['key'], $trackID)
                ),
                Filters::FILTER_KEY_WEATHER => array_map(
                    fn($weather) => ['name' => $weather['name'], 'value' => $weather['id']],
                    $this->getFilterOptions(Weather::class, $filter['key'], $trackID)
                ),
                Filters::FILTER_KEY_TYRE => array_map(
                    fn($tyre) => ['name' => $tyre['name'], 'value' => $tyre['id']],
                    $this->getFilterOptions(Tyre::class, $filter['key'], $trackID)
                ),
                Filters::FILTER_KEY_DRS => array_map(
                    fn($drsUsed) => ['name' => $drsUsed ? 'Yes' : 'No', 'value' => (int) $drsUsed],
                    Lap::query()
                        ->select('drs_used')
                        ->distinct()
                        ->where('track_id', $trackID)
                        ->get()
                        ->pluck('drs_used')
                        ->toArray()
                ),
                Filters::FILTER_KEY_OVERTAKE_BUTTON => array_map(
                    fn($overtakeButtonUsed) => ['name' => $overtakeButtonUsed ? 'Yes' : 'No', 'value' => (int) $overtakeButtonUsed],
                    Lap::query()
                        ->select('overtake_button_used')
                        ->distinct()
                        ->where('track_id', $trackID)
                        ->get()
                        ->pluck('overtake_button_used')
                        ->toArray()
                ),
                default => [],
            };

            return array_merge($filter, ['options' => $options]);
        }, Filters::all());
    }

    public function find(int $trackID, int $lapID)
    {
        $this->checkTrack($trackID);
        $lap = Lap::query()
            ->where('track_id', $trackID)
            ->where('id', $lapID)
            ->first();

        if ($lap === null) {
            throw new ModelNotFoundException('lap', $lapID);
        }
        return $lap;
    }

    public function create(int $trackID, Request $request)
    {
        $this->checkTrack($trackID);

        if (!$request->has('player_no') || !in_array($player_no = (int) $request->get('player_no'), [1, 2])) {
            throw new InvalidRequestException();
        }

        $session = $this->sessionService->getSession();
        if ($session->player_1_id === null || ($player_no === 2 && $session->player_2_id === null)) {
            throw new InvalidSessionException();
        }

        $lap = new Lap();
        $lap->fill($request->all());
        $lap->track_id = $trackID;
        $lap->player_id = $player_no === 1 ? $session->player_1_id : $session->player_2_id;
        $lap->driver_id = min($lap->driver_id, 100);
        $lap->saveOrFail();
        return response($lap->load(['player', 'driver', 'team', 'weather', 'formula', 'tyre']), 201);
    }

    public function update(int $trackID, int $lapID, Request $request)
    {
        $lap = $this->find($trackID, $lapID);
        $lap->fill($request->all());
        $lap->saveOrFail();
        return $lap->load(['player', 'driver', 'team', 'weather', 'formula', 'tyre']);
    }

    public function delete(int $trackID, int $lapID)
    {
        $lap = $this->find($trackID, $lapID);
        $lap->delete();
        return response('', 204);
    }

    private function checkTrack(int $trackID)
    {
        if (!Track::query()->where('id', $trackID)->exists()) {
            throw new ModelNotFoundException('track', $trackID);
        }
    }

    private function getFilterOptions(string $model, string $fk, int $trackID)
    {
        return $model::query()->distinct()->whereIn('id', function ($query) use ($trackID, $fk) {
            $query->select($fk)
                ->from('laps')
                ->where('track_id', $trackID);
        })->get()->toArray();
    }
}
