<?php

namespace App\Http\Controllers;

use App\Exceptions\IncorrectCredentialsException;
use App\Exceptions\InvalidRequestException;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller;

class AuthController extends Controller
{
    const JWT_EXPIRE_MINUTES = 30;

    public function login(Request $request)
    {
        if (!$request->has(['email', 'password'])) {
            throw new InvalidRequestException();
        }

        $user = User::query()->where('email', $request->get('email'))->first();
        if ($user === null || !Hash::check($request->get('password'), $user->password)) {
            throw new IncorrectCredentialsException();
        }

        return [
            'token' => $this->generateToken($user)
        ];
    }

    public function me(Request $request)
    {
        return $request->auth;
    }

    private function generateToken(User $user): string
    {
        $payload = [
            'iss' => 'f1.roelink.eu',
            'iat' => time(),
            'exp' => time() + self::JWT_EXPIRE_MINUTES * 60,
            'sub' => $user->id
        ];
        return JWT::encode($payload, env('JWT_SECRET'));
    }
}
