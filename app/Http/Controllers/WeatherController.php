<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseControllers\BaseReadController;
use App\Models\Weather;

class WeatherController extends BaseReadController
{
    public function __construct()
    {
        parent::__construct(Weather::class);
    }
}
