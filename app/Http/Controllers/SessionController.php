<?php

namespace App\Http\Controllers;

use App\Services\SessionService;
use Illuminate\Http\Request;

class SessionController
{
    private SessionService $sessionService;

    public function __construct(SessionService $sessionService)
    {
        $this->sessionService = $sessionService;
    }

    public function get()
    {
        return $this->sessionService->getSession();
    }

    public function set(Request $request)
    {
        $session = $this->sessionService->getSession();
        $session->fill($request->all());
        $session->saveOrFail();
        return $session->load(['player_1', 'player_2']);
    }
}
