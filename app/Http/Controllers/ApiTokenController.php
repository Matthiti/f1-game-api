<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseControllers\BaseCrudController;
use App\Models\ApiToken;
use Illuminate\Http\Request;

class ApiTokenController extends BaseCrudController
{
    public function __construct()
    {
        parent::__construct(ApiToken::class);
    }

    public function create(Request $request)
    {
        $token = $this->generateApiToken();

        $apiToken = new ApiToken();
        $apiToken->fill($request->all());
        $apiToken->token_hash = hash('sha256', $token);
        $apiToken->saveOrFail();

        # Return plaintext token
        return response(
            array_merge($apiToken->toArray(), ['token' => $token]),
            201
        );
    }

    private function generateApiToken()
    {
        return base64_encode(random_bytes(20));
    }
}
