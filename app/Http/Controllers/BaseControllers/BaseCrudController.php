<?php

namespace App\Http\Controllers\BaseControllers;

use Illuminate\Http\Request;

class BaseCrudController extends BaseReadController
{
    public function __construct(string $modelClass)
    {
        parent::__construct($modelClass);
    }

    public function create(Request $request)
    {
        $model = new $this->modelClass;
        $model->fill($request->all());
        $model->saveOrFail();
        return response($model, 201);
    }

    public function update(int $id, Request $request)
    {
        $model = $this->find($id);
        $model->fill($request->all());
        $model->saveOrFail();
        return $model;
    }

    public function delete(int $id)
    {
        $model = $this->find($id);
        $model->delete();
        return response('', 204);
    }
}
