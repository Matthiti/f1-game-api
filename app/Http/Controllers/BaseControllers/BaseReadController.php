<?php

namespace App\Http\Controllers\BaseControllers;

use App\Exceptions\ModelNotFoundException;
use Laravel\Lumen\Routing\Controller;

class BaseReadController extends Controller
{
    protected string $modelClass;

    public function __construct(string $modelClass)
    {
        $this->modelClass = $modelClass;
    }

    public function all()
    {
        return $this->modelClass::all();
    }

    public function find(int $id)
    {
        $model = $this->modelClass::query()->find($id);
        if ($model === null) {
            throw new ModelNotFoundException($this->getModelName(), $id);
        }
        return $model;
    }

    private function getModelName(): string
    {
        return strtolower(substr(strrchr($this->modelClass, '\\'), 1));
    }
}
