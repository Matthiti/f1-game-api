<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseControllers\BaseReadController;
use App\Models\Team;

class TeamController extends BaseReadController
{
    public function __construct()
    {
        parent::__construct(Team::class);
    }
}
