<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseControllers\BaseReadController;
use App\Models\Driver;

class DriverController extends BaseReadController
{
    public function __construct()
    {
        parent::__construct(Driver::class);
    }
}
