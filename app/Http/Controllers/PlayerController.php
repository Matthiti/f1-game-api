<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseControllers\BaseCrudController;
use App\Models\Player;

class PlayerController extends BaseCrudController
{
    public function __construct()
    {
        parent::__construct(Player::class);
    }
}
