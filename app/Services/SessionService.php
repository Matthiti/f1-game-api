<?php

namespace App\Services;

use App\Models\Session;

class SessionService
{
    public function getSession()
    {
        return Session::query()->first();
    }
}
