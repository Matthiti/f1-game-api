<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateUserCommand extends Command
{
    protected $signature = 'user:create {--first_name=} {--last_name=} {--email=} {--password=}';

    protected $description = 'Inserts a user into the database';

    public function handle()
    {
        $first_name = $this->option('first_name');
        $last_name = $this->option('last_name');
        $email = $this->option('email');
        $password = $this->option('password');

        while (empty($first_name)) {
            $first_name = $this->ask('First name');
        }

        while (empty($last_name)) {
            $last_name = $this->ask('Last name');
        }

        while (empty($email)) {
            $email = $this->ask('Email');
        }

        while (empty($password)) {
            $password = $this->ask('Password');
        }

        if (User::query()->where('email', $email)->exists()) {
            $this->error("User with email $email already exists");
            return;
        }

        $user = new User();
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->email = $email;
        $user->password = Hash::make($password);
        $user->save();

        $this->info("User with id $user->id created successfully");
    }
}
