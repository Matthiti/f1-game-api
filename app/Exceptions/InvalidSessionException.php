<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class InvalidSessionException extends HttpException
{
    public function __construct()
    {
        parent::__construct(409, 'Invalid session');
    }
}
