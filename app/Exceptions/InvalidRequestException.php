<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class InvalidRequestException extends HttpException
{
    public function __construct()
    {
        parent::__construct(400, 'Invalid request body');
    }
}
