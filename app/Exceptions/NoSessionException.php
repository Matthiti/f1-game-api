<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class NoSessionException extends HttpException
{
    public function __construct()
    {
        parent::__construct(409, 'There is no session');
    }
}
