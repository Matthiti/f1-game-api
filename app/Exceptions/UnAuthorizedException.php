<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class UnAuthorizedException extends HttpException
{
    public function __construct(string $message)
    {
        parent::__construct(401, $message);
    }
}
