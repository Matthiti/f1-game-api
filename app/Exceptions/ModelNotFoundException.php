<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ModelNotFoundException extends HttpException
{
    public function __construct(string $model, int $id)
    {
        $model = ucfirst($model);
        parent::__construct(404, "$model with id $id not found");
    }
}
