<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ExistingSessionException extends HttpException
{
    public function __construct()
    {
        parent::__construct(409, 'There already exists a session');
    }
}
