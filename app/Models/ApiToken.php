<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class ApiToken extends Model
{
    use ValidatingTrait;

    const UPDATED_AT = null;

    protected $table = 'api_tokens';

    public $timestamps = ['created_at'];

    protected $dateFormat = 'U';

    protected $fillable = ['name'];

    protected ?array $rules = [
        'name' => 'required'
    ];

    protected $hidden = ['token_hash'];

    function getCreatedAtAttribute($value) {
        return intval(date('U', strtotime($value)));
    }
}
