<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class Player extends Model
{
    use ValidatingTrait;

    protected $table = 'players';

    public $timestamps = false;

    protected $fillable = ['username'];

    protected ?array $rules = [
        'username' => 'required|unique:players,username'
    ];
}
