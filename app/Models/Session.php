<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class Session extends Model
{
    use ValidatingTrait;

    protected $table = 'sessions';

    public $timestamps = false;

    protected $fillable = ['player_1_id', 'player_2_id'];

    protected ?array $rules = [
        'player_1_id' => 'nullable|integer|exists:players,id',
        'player_2_id' => 'nullable|integer|exists:players,id'
//        'player_2_id' => 'nullable|integer|exists:players,id|different:player_1_id'
    ];

    protected $hidden = ['id', 'player_1_id', 'player_2_id'];

    protected $with = ['player_1', 'player_2'];

    public function player_1()
    {
        return $this->belongsTo(Player::class, 'player_1_id');
    }

    public function player_2()
    {
        return $this->belongsTo(Player::class, 'player_2_id');
    }
}
