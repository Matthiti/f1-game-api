<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class Lap extends Model
{
    use ValidatingTrait;

    protected $table = 'laps';

    public $timestamps = false;

    protected $casts = [
        'drs_used' => 'boolean',
        'overtake_button_used' => 'boolean'
    ];

    protected $fillable = [
        'lap_no',
        'time',
        'time_sector_1',
        'time_sector_2',
        'time_sector_3',
        'timestamp',
        'player_id',
        'driver_id',
        'team_id',
        'weather_id',
        'formula_id',
        'session_type_id',
        'tyre_id',
        'tyre_age',
        'drs_used',
        'overtake_button_used'
    ];

    protected ?array $rules = [
        'lap_no' => 'required|integer|min:1',
        'time' => 'required|numeric|min:0',
        'time_sector_1' => 'required|numeric|min:0',
        'time_sector_2' => 'required|numeric|min:0',
        'time_sector_3' => 'required|numeric|min:0',
        'timestamp' => 'required|integer|min:0',
        'track_id' => 'required|integer|exists:tracks,id',
        'player_id' => 'required|integer|exists:players,id',
        'driver_id' => 'required|integer|exists:drivers,id',
        'team_id' => 'required|integer|exists:teams,id',
        'weather_id' => 'required|integer|exists:weather,id',
        'formula_id' => 'required|integer|exists:formulas,id',
        'session_type_id' => 'required|integer|exists:session_types,id',
        'tyre_id' => 'required|integer|exists:tyres,id',
        'tyre_age' => 'required|integer|min:0',
        'drs_used' => 'required|boolean',
        'overtake_button_used' => 'required|boolean'
    ];

    protected $hidden = ['track_id', 'player_id', 'driver_id', 'team_id', 'weather_id', 'formula_id', 'session_type_id', 'tyre_id'];

    protected $with = ['player', 'driver', 'team', 'weather', 'formula', 'session_type', 'tyre'];

    public function player()
    {
        return $this->belongsTo(Player::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function weather()
    {
        return $this->belongsTo(Weather::class);
    }

    public function formula()
    {
        return $this->belongsTo(Formula::class);
    }

    public function session_type()
    {
        return $this->belongsTo(SessionType::class);
    }

    public function tyre()
    {
        return $this->belongsTo(Tyre::class);
    }
}
