<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tyre extends Model
{
    protected $table = 'tyres';

    public $timestamps = false;

    protected $with = ['formula'];

    protected $hidden = ['formula_id'];

    public function formula()
    {
        return $this->belongsTo(Formula::class);
    }
}
