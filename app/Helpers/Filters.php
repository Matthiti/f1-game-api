<?php

namespace App\Helpers;

class Filters
{

    public const FILTER_KEY_PLAYER = 'player_id';
    public const FILTER_KEY_DRIVER = 'driver_id';
    public const FILTER_KEY_TEAM = 'team_id';
    public const FILTER_KEY_FORMULA = 'formula_id';
    public const FILTER_KEY_SESSION_TYPE = 'session_type_id';
    public const FILTER_KEY_WEATHER = 'weather_id';
    public const FILTER_KEY_TYRE = 'tyre_id';
    public const FILTER_KEY_DRS = 'drs_used';
    public const FILTER_KEY_OVERTAKE_BUTTON = 'overtake_button_used';

    private static ?array $filters = null;

    private static function initialize()
    {
        self::$filters = [
            [
                'name' => 'Players',
                'key' => self::FILTER_KEY_PLAYER,
                'multiple' => true
            ],
            [
                'name' => 'Drivers',
                'key' => self::FILTER_KEY_DRIVER,
                'multiple' => true
            ],
            [
                'name' => 'Teams',
                'key' => self::FILTER_KEY_TEAM,
                'multiple' => true
            ],
            [
                'name' => 'Formulas',
                'key' => self::FILTER_KEY_FORMULA,
                'multiple' => true
            ],
            [
                'name' => 'Session types',
                'key' => self::FILTER_KEY_SESSION_TYPE,
                'multiple' => true
            ],
            [
                'name' => 'Weather',
                'key' => self::FILTER_KEY_WEATHER,
                'multiple' => true
            ],
            [
                'name' => 'Tyres',
                'key' => self::FILTER_KEY_TYRE,
                'multiple' => true
            ],
            [
                'name' => 'DRS',
                'key' => self::FILTER_KEY_DRS,
                'multiple' => false
            ],
            [
                'name' => 'Overtake button',
                'key' => self::FILTER_KEY_OVERTAKE_BUTTON,
                'multiple' => false
            ]
        ];
    }

    private static function initialize_if_necessary()
    {
        if (null === self::$filters) {
            self::initialize();
        }
    }

    public static function all(): array
    {
        self::initialize_if_necessary();
        return self::$filters;
    }

    public static function keys(): array
    {
        return array_map(fn ($filter) => $filter['key'], self::all());
    }
}
